! ==================== !
! Programming Practice !
! ==================== !
! Venkata Krisshna

! Transpose matrix

program transpose_matrix
  implicit none
  integer,dimension(:,:),allocatable :: matrix
  integer :: dim(2)
  
  print*,'======================================='
  print*,'Programming Practice - Transpose matrix'
  print*,'======================================='
  call init
  call transpose
  
contains

  ! Read matrix
  subroutine init
    implicit none
    
    open(21, file="matrix")
    read(21,*) dim
    allocate(matrix(dim(2),dim(1)))
    read(21,*) matrix
    print*,'| ==== ORIGINAL MATRIX ==== |'
    call print(dim(2),dim(1),matrix)

    return
  end subroutine init

  ! Tranpose the matrix
  subroutine transpose
    implicit none
    integer,dimension(dim(1),dim(2)) :: trans
    integer :: i,j

    trans=0
    do i = 1,dim(2)
       do j = 1,dim(1)
          trans(j,i) = matrix(i,j)
       end do
    end do
    print*,'| ==== TRANSPOSED MATRIX ==== |'
    call print(dim(1),dim(2),trans)

    return
  end subroutine transpose

  ! Print matrix
  subroutine print(i,j,job)
    implicit none
    integer :: i,j,dj,job(i,j)

    do dj = 1,j
       print*,job(:,dj)
    end do
    
    return
  end subroutine print
  
end program transpose_matrix
